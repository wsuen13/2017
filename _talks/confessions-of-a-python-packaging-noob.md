---
abstract: "Learning to package Python modules for distribution wasn\u2019t easy. Watching\
  \ my repo become monolithic and unmanageable was even harder, so I recently bit\
  \ the bullet and created my first package. I\u2019ll share what I learned along\
  \ the way."
duration: 25
level: All
room: Madison
slot: 2017-10-06 10:15:00-04:00
speakers:
- Steven Saporta
title: Confessions of a Python packaging noob
type: talk
---

When my source code repo become too large, I decided to split some modules out into a separate package, which can be installed from GitHub or other repositories using pip. Along the way, I learned about:
-The difference between a package and a module
-Setuptools
-The setup.py file
-Using a requirements.txt file in a package
-Recommended directory structure for a package
-Package versioning
-PyPI, the Python Package Index
-Labeling a package version on GitHub
-Using pip to install a package from GitHub
 
I’ll share my packaging journey and provide links to helpful documentation. This talk is suitable for a moderately technical audience. Programmers and non-programmers alike will gain a greater understanding of Python packaging.