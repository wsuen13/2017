---
abstract: "You don\u2019t have to know python to tell that the trains are getting\
  \ worse. Fortunately, a wealth of data is available for us to predict how bad trains\
  \ are likely to be at any particular time. While we can\u2019t fix the trains, we\
  \ can identify factors that make delays more likely and work around them."
duration: 25
level: All
room: PennTop North
slot: 2017-10-07 15:10:00-04:00
speakers:
- josh laurito
title: 'When to avoid the subway: using python to make your commute slightly less
  terrible'
type: talk
---

While there are already great tools for real time analysis of traffic, figuring out the value of waking up 15 minutes early isn't always obvious. But we can figure out times and days that are most likely to have breakdowns as delays on our own,  and use this to improve our commute.

Primarily,  this talk is about:

- setting up a system to gather unique information about train performance
- finding factors that correlate with delays and complaints about poor train experiences
- using this information to find the best times to avoid,  and where there's the most value in waking up early to commute
- making this information accessible and convenient for me and my friends/ co-workers