---
title: Young Coders
---

Ever wonder how web sites and games are made? If you are 10 years old or over,
join us for a few hours on Sunday, October 8th to explore how to program, and
why programmers love Python so much.

Python is a very powerful programming language: it is used for writing computer
applications, making websites and games, scientific and mathematical computing,
and much more. Some of the companies that use Python include: Google, NASA,
Disney Animation, and Spotify.

This free tutorial will explore how to program using Python starting with the
basics: math and words, comparisons, loops, and kinds of data. Finally, we will
combine our new knowledge by using our skills to do some fun robotics projects
with the BBC micro:bit.

We'll provide computers with Python and everything else you need already
installed, and every student will get to take home their own micro:bit.

Registration for this event is open! Students must be registered by a parent
or guardian, and each student must bring a release form signed by a parent or
guardian to be allowed into the class. A link to that form is provided at the
registration page. (If you are interested in helping out as a teacher's
assistant, you'll be able to sign up at the same registration page.)

<https://www.eventbrite.com/e/pygotham-2017-young-coders-registration-35223990905>

## About the instructors

Barbara Shaurette is an open source veteran, with 20 years of experience as a
professional developer, much of that as an active participant in the Python and
Django communities. She teaches and mentors through organizations such as
PyLadies, Girl Develop It and DjangoGirls. Currently, she works as a Python
developer for Vox Media Group. Learn more about what Barbara is up to at her
blog, mechanicalgirl.com.

Meg Ray is the Teacher in Residence at Cornell Tech, where she coaches and
trains K-8 public school educators to teach computer science. Previously, she
was a classroom computer science teacher and special educator in NYC public
schools, as well as an adjunct lecturer in the School of Ed. at Hunter College.
She also designed and developed middle school Python curricula for Codesters.
Meg served as a writer for the Computer Science Teachers’ Association K-12 CS
Standards and as a special advisor to the K12 Framework for CS.
