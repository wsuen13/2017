---
name: Dustin Ingram
talks:
- 'The Fastest FizzBuzz in the West: How to Make Your Own Programming Language'
---

I’m Dustin (aka [@di](<https://github.com/di>)), a software engineer at PromptWorks, the premier Philadelphia software consulting shop.

I have been a professional Python developer for more than ten years and have authored a number of small open-source projects (<https://github.com/di>) including a number of Python packages (<https://pypi.org/user/di/>).

I'm a member of the Python Packaging Working Group, the Python Packaging Authority (<https://github.com/orgs/pypa/people)> and a maintainer of the Warehouse project (<https://pypi.org/>).