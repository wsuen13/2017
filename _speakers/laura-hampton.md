---
name: Laura Hampton
talks:
- 'An Introduction to the Raft Distributed Consensus Algorithm '
---

Laura Hampton is a Python developer living in New York City.