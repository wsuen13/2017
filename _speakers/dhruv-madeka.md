---
name: Dhruv Madeka
talks:
- bqplot - Seamless Interactive Visualizations in the Jupyter Notebook
---

Dhruv Madeka is a Quantitative Researcher at Bloomberg LP. His current research interests focus on Machine Learning, Data Visualization and Applied Mathematics. Having graduated from the University of Michigan with a BS in Operations Research and from Boston University with an MS in Mathematical Finance, Dhruv is part of one of the leading research teams in Finance, developing models, software and tools for users to make their data analysis experience richer.