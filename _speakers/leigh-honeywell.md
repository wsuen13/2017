---
name: Leigh Honeywell
talks:
- 'Halfway Between Heaven and Hell: securing grassroots groups'
---

Leigh Honeywell is a Technology Fellow at the ACLU. Prior to the ACLU, she
worked at Slack, Salesforce.com, Microsoft, and Symantec. She has co-founded two
hackerspaces, and is an advisor to several nonprofits and startups. Leigh has a
Bachelors of Science from the University of Toronto where she majored in
Computer Science and Equity Studies.
