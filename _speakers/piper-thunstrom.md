---
name: Piper Thunstrom
talks:
- 'Monkey Patching My Life: Being a Trans Python Developer'
---

A transgender Pythonista with a passion for games. Professionally a web platform engineer, in her free time she builds game libraries and encourages people to pick up a little Python.