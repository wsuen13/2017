---
title: "Announcing our second keynote: David Beazley"
date: 2017-08-24 12:00:00 -0400
image: /assets/david-beazley.jpg
excerpt_separator: <!--more-->
---

David Beazley is the author of the [Python
Cookbook](http://www.dabeaz.com/cookbook.html) (O'Reilly Media) and the [Python
Essential Reference](http://www.dabeaz.com/per.html) (Addison-Wesley). He's been
involved with Python since 1996.
